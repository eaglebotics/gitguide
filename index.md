# How to Use Git and GitHub From Start to Finish (written by Myl0g)

# This Guide has been replaced.

See [this](https://medium.com/@eaglebotics/how-to-use-github-db771297f84f) one instead.

---

This guide shows the basics of Git and how to use GitHub. 

## Of Note

You can find a glossary of terms at the bottom of this page.

All of the commands listed in this guide were tested on a Mac Terminal (/Applications/Utilites/Terminal.app). Commands for Git will not vary on a Windows machine, but other commands not related to Git will vary.

## Installing Git

Before you use Git, it needs to be installed on your system.

However, if you plan to only use Git with Eclipse, you can simply scroll down to the section on eGit, no Git installation required.

### Standard Installation (Mac)

If you have already installed Xcode, Git is installed on your Mac.

If Xcode isn't installed, run the following command:

```markdown
git
```

This should prompt your OS to ask you to install the software.

Then, install [Github Desktop](https://desktop.github.com).

### Alternate Installation (Mac)

If the above method doesn't work, you'll need to install Git with Homebrew. Run the following commands:

```markdown
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install git
brew cask install github
```

This installs both Git and Github Desktop with Homebrew, a macOS package manager.

### Standard Installation (Windows)

To install Git on Windows, install [GitHub Desktop](https://desktop.github.com/).

Once it's installed, login with your GitHub account.

Then, find the application "Git Shell" in the Start Menu.

### Alternate Installation (Windows)

If you can't use the Git Shell or Github Desktop, install Git from their official website [here](http://git-scm.com/download/win).

### Installation (Linux)

Use your distribution's package manager to install Git and Github Desktop.

Alternativley, install the correct tarball [here](https://www.kernel.org/pub/software/scm/git/).

## Making Repositories

Git operates in folders called repositories. Any folder can be turned into a repository, and Git can only work inside repositories.

### Making a Local Repository (Skip if You Already Have One)

Before beginning, install [GitHub Desktop](https://desktop.github.com/) and login with your GitHub account. This will ensure that you can interact with GitHub from the command line, which will become important later on in this guide.

First, make a folder for your new repository. This is how it would be done on Mac:

```
mkdir testrepository
```

Then, navigate inside the folder and "initialize" a repository:

```
cd testrepository
git init
```

This will create a new folder inside of the folder testrepository named .git. The .git folder is what identifies a git repository and stores all the information related to the repository.

However, your repository only exists on your computer. To take advantage of Git's collaboration features, you'll need to establish a remote repository as well. This is where GitHub comes into play.

#### Making a GitHub Repository and Syncing it with the Local Repository

Now, make a corresponding repository on GitHub, with the same name as your local repository. This new GitHub repository will act as your remote repository.

![Getting your remote repository link](yourlink.png)

Once you have made a new GitHub repository, copy the link provided (looks something like github.com/Username/repository.git) and use this command (where "yourlink" is the link you copied):

```
git remote add origin yourlink
git push -u origin master
```

This will sync up the GitHub repository and the repository on your computer, and establish the GitHub repository as the "origin" repository, which means that when you ask your computer to push and pull data, it will do it from the GitHub repository.

## Using Git to Track Changes

While Git is great for collaborating on code, that is just one of its features. Git also boasts the ability to track changes through something called "committing." When you commit to a repository, you tell Git to take a snapshot of everything inside the repository. 

### Basic Change Tracking

#### Github Desktop (Recommended)

When you open Github Desktop, if you can't already see your repository, choose File -> Add Local Repository and choose it there.

Changes made to files will be automatically detected whenever you open/switch to Github Desktop.

You can then use the tab right under the list of changed files to commit your changes. Please be as descriptive as possible and make use of both the Commit Message and Description sections.

Ideally, your commits will pertain to specific issues (i.e. one commit to fix a typo, another commit to add code for autonomous, etc.). You'll learn about better change-tracking and organization in the coming sections

#### Command Line (Alternate to Github Desktop)

First, you'll add a file to something called the staging area. When you tell Git to commit, it will only commit files in the staging area, ignoring any files not in the staging area. 

![Staging Area](staging.png)

To add a specific file to the staging area, use this command:

```
git add filename
```

Most of the time, however, you'll need to add all the files in a repository to the staging area. To do that, use this command:

```
git add -A
```

Now that your files are in the staging area, you can commit. Git will record all of your files' states as they were when you added them to the staging area.

```
git commit -m "Place a description of your commit in-between these quotes."
```

A handy way to kill two birds with one stone would look like this: 

```
git commit -a -m "Message"
```

Note that the above command automatically stages all files that have been modified or deleted, but not new files. Use git add for new files.

To see a list of all your commits, use this command:

```
git log
```

You should see a list of every commit's ID (looks like a bunch of random numbers), author, and timestamp.

Now comes the interesting part: rolling back changes by reverting to a previous commit. To do that, all you need is the ID of the commit (use git log and then copy the numbers) and this command (where "id" is the ID of the commit you'd like to revert to):

```
git reset --hard id
```

Keep in mind that this will roll back your repository to the state it was in when that commit was made, and that the command is irreversible. There are safer, reversible ways to roll back changes to a repository, but they won't be detailed here.

### Making Branches

Think of Git branches the same way you would think of forks on a road: they start off the same way, but go separate directions. The concept is the same with Git branches: when a branch is created, it is the exact same as the master branch (the very first branch, and the one that you committed to in the previous section of this tutorial) but seperate changes can be made to seperate branches. You typically use branches other than the master branch when working on code you aren't sure will work, since the master branch is typically reserved for stable code.

#### Github Desktop (Recommended)

In the top bar, choose "Current Branch - master".  On the top right is a "New" button. Ideally, you'd name your branch something descriptive for the changes you're making.

Once you have swithced to a branch, any commits you make will apply only to that branch. So, for example, if you make a new text file in one branch and commit, it won't show up in another.

#### Command Line (Alternate to Github Desktop)

To create a new branch, use this command (where branchname is the name of the branch you want to create):

```
git branch branchname
```

That command doesn't, however, switch you to that branch. This command will do that (where branchname is the name of the branch you want to switch to):

```
git checkout branchname
```

Once you have swithced to a branch, any commits you make will apply only to that branch. So, for example, if you make a new text file in one branch and commit, it won't show up in another.

### Merging Branches Automatically

#### Github Desktop

First switch to the branch you'd like to merge into (in our case, master).

Choose File->Branch->Merge Into Current Branch and choose your branch.

#### Command Line

Combining multiple branches is called merging. To merge a branch with the master branch, use these commands (where branchname is the branch you want to merge with the master branch):

```
git checkout master
git merge branchname
```

Merging branches is a very simple process if the differences between the two branches aren't vast. For example, if the master branch contains a text file containing the word "Hello" on line 1, and another branch contains the word "Hello" on line 1 and the word "world" on line 2, merging can happen automatically because the only thing Git has to do is get the master branch up to speed with the new changes. This is called fast-forwarding.

### Merging Branches Manually

Sometimes, however, merging can't happen automatically because Git can't simply fast-forward. It will, instead, display an error message:

```markdown
CONFLICT (content): Merge conflict in filename
Automatic merge failed; fix conflicts and then commit the result.
```

This is called a merge conflict. When this happens, Git will place markers inside the master branch files which can't be successfully merged to indicate where the issues are. These markers are less-than symbols, multiple equal signs, and greater than symbols. Everything in-between the less-than symbols and equal signs is content from the master branch, and everything in-between the equal signs and greater-than symbols is content from the other branch.

To resolve a merge conflict, just edit the content of the affected files to your liking, keeping certain aspects of different branches if you'd like. Just make sure to remove the markers after you finish. Then, add everything to the staging area and commit the result.

## Using Git (and GitHub) to Collaborate

A key aspect of Git is the ability to collaborate. This section details how to push changes to the remote repository on GitHub, how to get new changes that others pushed to the repository, and how to add collaborators to your repository.

### Pushing and Pulling Changes from Remote Repository

On Github Desktop, just choose "Fetch Origin" (it will change to "Pull") to recieve changes.

To receive any changes made to the remote repository that aren't on your computer, use this command:

```markdown
git pull
```

To push any changes you made to your local repository to your remote repository, use this command:

```markdown
git push
```

### Adding Collaborators to Your GitHub Repository

Adding people to your GitHub repository is like sharing a file in Google Drive.

1. Navigate to your repository's setting's page (github.com/User/repositoryname/settings)
2. Click on "Collaborators".
3. Re-enter your password if asked.
4. Enter in a e-mail address or GitHub username.

![Collaborator adding](collab.png)

## Using Git with Eclipse (eGit)

There is a special add-on to Eclipse that integrates it with Git: eGit. This section details how to install and use eGit.

### Installing eGit

1. Navigate to Help -> Install New Software (in the menu bar)
2. Provide the following URL: http://download.eclipse.org/egit/updates/
3. Check all boxes execpt for "Git integration for Eclipse - experimental features"
4. Proceed with the installation by clicking "Next"

![Install New Software](eclipseinstall.png)

### Using eGit

To use eGit with Eclipse, simply right-click on the project folder (must be a repository first; use git init and git remote add) and navigate to the "Team" option.

# Issues

If there are any issues with this guide, open up an [issue](https://github.com/NBPSEaglebotics/gitguide/issues).

# Glossary (in order of appearance)

- Git: change-tracking and collaboration software most commonly used for code.
- Working Directory: directory in which you work (e.g. the folder you keep your code in).
- mkdir: Mac and Linux command which creates a folder with the specified name.
- git push: Tells Git to upload changes made on a local repository to a remote repository.
- git add: Adds files to the staging area.
- Staging Area: Virtual area which Git uses to commit to the repository.
- git commit: Tells git to take a snapshot of all the items in the staging area, saving the data in the .git folder.
- Branch: separately tracked version of the working directory.
- master: the main branch created by Git when initializing a repository.
- HEAD: special data marker used by Git to remember what place in history you are at. Modified by several commands (namely checkout and reset)
- git checkout: Switches to a different branch (name must be specified).
- git merge: merges two branches back into one, typically by fast-forwarding. This command should be executed from the master branch for organizational purposes.
- git pull: receives new changes from remote repository, then incorporates then into the local repository.
- eGit: plug-in for Eclipse (Juno or newer) which provides Git functionality within Eclipse. Built into the GUI, no console commands required.
